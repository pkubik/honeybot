#!/usr/bin/python2

import socket
import sys
import re
import json
import io
import signal
import os

DEFAULT_CONFIG_PATH = "config.json"
DEFAULT_LOG_PATH = None

class Bot:
    def __init__(self, conf_path=DEFAULT_CONFIG_PATH, log_path=DEFAULT_LOG_PATH):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conf = json.load(open(conf_path))
        self.conf_path = conf_path
        if log_path is None:
            self.log = sys.stdout
        else:
            self.log = io.open(log_path, "a")
        self.lastconfmod = os.path.getmtime(self.conf_path)

    def connect(self):
        try:
            self.server.connect((self.conf["address"], self.conf["port"]))
            for cmd in self.conf["init"]:
                self.server.send(cmd)
        except KeyError, e:
            self.log.write(unicode("!!!!!!: Configuration file is missing '" + e.message + "' variable.\n"))
        except socket.error, e:
            self.log.write(unicode("!!!!!!: Exception - '" + e.message + "'.\n"))

    def form_response(self, groups, pattern):
        r = re.split("%{|}", pattern)
        try:
            r[1::2] = [groups[int(i)] for i in r[1::2]]
        except ValueError:
            self.log.write(unicode("!!!!!!: Invalid pattern - brackets should contain a number.\n"))
        except IndexError:
            self.log.write(unicode("!!!!!!: The index of requested group is to high. REGEX conain only " + len(groups) + " groups.\n"))
        return "".join(r) + self.conf["endl"]

    # important note: python >=2.5 is caching all regex matches
    def get_respose(self, cmd):
        for p in self.conf["patterns"]:
            m = re.match(p[0], cmd)
            if m is not None:
                return self.form_response(m.groups(), p[1]), True
        return "", False

    def respond(self):
        try:
            text = self.server.recv(2048)
            if self.lastconfmod != os.path.getmtime(self.conf_path):
                self.log.write(unicode("!!!!!!: Updating config file...\n"))
                self.conf = json.load(open(self.conf_path))
                self.lastconfmod = os.path.getmtime(self.conf_path)
            for line in text.split(self.conf["endl"])[:-1]:
                self.log.write("SERVER: " + line + self.conf["endl"])
                response,success = self.get_respose(line)
                if success:
                    self.log.write("CLIENT: " + response)
                    self.server.send(response)
            self.log.flush()
        except socket.error, e:
            if e.args[0] != 4:
                self.log.write(unicode("!!!!!!: Exception - '" + e.args[1] + "'.\n"))
                self.stopLoop()


    def loop(self):
        self.running = True
        try:
            while self.running:
                self.respond()
        except KeyError, e:
            self.log.write(unicode("!!!!!!: Configuration file is missing '" + e.message + "' variable.\n"))

    def stopLoop(self):
        if self.running:
            self.running = False
            self.server.close()

def main():
    conf_path = DEFAULT_CONFIG_PATH
    log_path = DEFAULT_LOG_PATH
    if len(sys.argv) >= 2:
        conf_path = sys.argv[1]
    if len(sys.argv) >= 3:
        log_path = sys.argv[2]
    bot = Bot(conf_path, log_path)

    def signal_handler(signal, frame):
        print('\nDisconnecting...')
        bot.stopLoop();

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    bot.connect()
    bot.loop()

if __name__ == "__main__":
    main()

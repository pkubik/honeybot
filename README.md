**HoneyPot bot**
==============
Sposób działania
----------------
Skrypt nawiązuje połączenie z serwerem i wchodzi do pętli obsługującej zapytania serwera C&C. Komunikacja jest realizowana przy pomocy standardowego modułu socket. Ogólny schemat przedstawiony jest na schemacie blokowym.

![Schemat blokowy](/bot.svg?raw=true)

Konfiguracja
------------
Przykładowy plik konfiguracyjny:
```python2
{
    "address": "localhost",                        #adres serwera
    "port": 6666,                                  #port
    "endl": "\r\n",                                #znak końca linii
    "init": [                                      #seria początkowych poleceń
        "USER botname botname botname :Bot\r\n",
        "NICK botname\r\n",
        "JOIN #CHANELS\r\n"
    ],
    "patterns": [                                  #wzorce poleceń i odpowiedzi
        ["PING ([^ ]*)$", "PONG %{0}"],
        ["TEST (.*) ([0-9]*)$", "ANS %{1} %{0} %{0}%{1}"]
    ]
}
```

* Znak końca linii `endl` odróżnia poszczególne komendy serwera - każda komenda musi kończyć się tym znakiem. W przeciwnym wypadku jest ignorowana
* `init` to lista poleceń wysyłanych do serwera przy inicjacji połączenia
* jeśli jakieś polecenie wymaga wielu odpowiedzi, należy użyć znaku końca linii

Każde polecenie jest reprezentowane poprzez sekwencję par składających się z wzorca polecenia oraz wzorca odpowiedzi. Program sekwencyjnie przegląda wszystkie wzorce, dopóki nie trafi na ten pasujący do polecenia, a następnie wysyła do serwera odpowiedź przyporządkowaną do dopasowanego wzorca. W niektórych przypadkach może mieć zatem znaczenie kolejność umieszczenia wzorców w pliku konfiguracyjnym. Jeśli polecenie nie zostanie dopasowane do żadnego wzorca zostanie zignorowane.

Uruchamianie
------------
Wykonać polecenie:
```sh
$ python2 client.py [conf] [log]
```
lub
```sh
$ ./client.py [conf] [log]
```
po uprzednim nadaniu uprawnień do uruchamiania, gdzie:

* **conf** - plik konfiguracyjny, opcjonalny, domyślnie `config.json`
* **log** - plik wyjściowy logów, opcjonalny, domyślnie `STDOUT`


Program obsługuje sygnały `SIGINT` i `SIGTERM` do łagodnego wyjścia, ale regularnie opróżnia bufor zapisu, więc w przypadku awarii zostanie utracony co najwyżej ostatni log.

Szybka procedura testująca korzystając z REPL
---------------------------------------------
Uruchomienie serwera (testowy C&C):
```python2
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 6666))
s.listen(5)
```
W tym momencie należy uruchomić klienta omówionym wyżej sposobem i zaakceptować jego połączenie:
```python2
c,a = s.accept()
```
Następnie można dowolnie naśladować zachowanie serwera wykonując na przemian:
```python2
c.recv(2000)
c.send("TEST ABC 123\r\n")
```
gdzie jako argument funkcji `send` należy podawać testowe zapytanie. **Konieczny jest znak końca linii!**
